# 90477_mlds_ii_project_template

This is the template you are going to use for the project.

The `90477_mlds_ii_project_template` repository has to contain the following folders:

1. `scripts` that contains `python` scripts in `.py` format or jupyter notebook files in `.ipynb` format
1. `data`, if necessary, that contains input files, usually in the `.csv` format
1. `results` that contains output files, usually in the `.csv` format
1. `figures` that contains plot files, usually in the `.png` format
1. `references` that contains the project's paper and other documents usuful for the development of the project

Each folder contains a `README.md` file, where you can add a description of the folder's content.

Please add the project number when you create your project repository in gitlab, for example:

1. `90477_mlds_ii_PR1` for `PR1`
1. `90477_mlds_ii_PR50` for `PR50`


